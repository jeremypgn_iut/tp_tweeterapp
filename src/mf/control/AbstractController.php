<?php
namespace mf\control;

abstract class AbstractController {
  
  /* Attribut pour stocker l'objet HttpRequest */ 
  protected $request=null;
  protected $router=null;
  
  /*
   * Constructeur :
   * 
   * Crée une instance de la classe HttpRequest et la stocke dans l'attribut
   *    $request 
   *
   */
  
  public function __construct($router){
      $this->request = new \mf\utils\HttpRequest() ;
      $this->router = $router;
  }
  
}


  
