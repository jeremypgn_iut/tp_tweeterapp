<?php
namespace mf\router;

use mf\auth\Authentification;
use mf\utils\HttpRequest;
use tweeterapp\auth\TweeterAuthentification;

class Router extends AbstractRouter{

    public function __construct()
    {
        parent::__construct();
    }

    public function run(){
        if(key_exists($this->http_req->path_info, self::$routes)){

            $auth = new Authentification();
            if($auth->checkAccessRight(self::$routes[$this->http_req->path_info][2], $auth->access_level)){
                $controller = self::$routes[$this->http_req->path_info][0];
                $method = self::$routes[$this->http_req->path_info][1];

                $ctrl = new $controller($this);
                $ctrl->$method();
            }else{
                $this->executeRoute(self::$aliases["default"]);
            }

        }elseif($this->http_req->path_info == "/"){
            $this->executeRoute(self::$aliases["default"]);
        }else{
            die("404"); // temporaire
        }
    }

    public function urlFor($route_name, $param_list = []){
        if(key_exists($route_name, $this::$aliases)){
            $url = $this->http_req->root."/main.php".$this::$aliases[$route_name];
            if(!empty($param_list)){
                $url .= "?".http_build_query($param_list);
            }
            return $url;
        }else{
            throw new \Exception("La route n'existe pas");
        }
    }

    public function executeRoute($alias){
        $controller = self::$routes[$alias][0];
        $method = self::$routes[$alias][1];
        $ctrl = new $controller($this);
        $ctrl->$method();
    }

    public function setDefaultRoute($url){
        self::$aliases["default"] = $url;
    }

    public function addRoute($name, $url, $ctrl, $mth, $requestedLevel =  TweeterAuthentification::ACCESS_LEVEL_NONE){
        self::$routes[$url] = [$ctrl, $mth, $requestedLevel];
        self::$aliases[$name] = $url;
    }
    public function getRoot(){
        return $this->http_req->root;
    }
}