<?php
namespace mf\utils;

class ClassLoader{
    private $prefix;

    public function __construct($prefix)
    {
        $this->prefix = $prefix;
    }

    public function test_autoload($class){
        $file = $this->prefix."/".str_replace("\\", DIRECTORY_SEPARATOR, $class).".php";
        if(file_exists($file)){
            require_once $file;
        }
    }

    public function register(){
        spl_autoload_register([$this, 'test_autoload']);
    }
}