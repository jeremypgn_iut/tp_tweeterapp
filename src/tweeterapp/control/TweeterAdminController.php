<?php
namespace tweeterapp\control;

use Illuminate\Support\Facades\Auth;
use mf\auth\Authentification;
use mf\control\AbstractController;
use tweeterapp\auth\TweeterAuthentification;
use tweeterapp\model\Follow;
use tweeterapp\model\User;
use tweeterapp\view\TweeterView;

class TweeterAdminController extends AbstractController {
    public function __construct($router){
        parent::__construct($router);

    }

    public function viewLogin(){
        $v = new TweeterView([], $this->router);
        $v->render("renderLogin");
    }

    public function checkLogin(){

        $auth = new TweeterAuthentification();


        try {
            $auth->loginUser($_POST["username"], $_POST["password"]);
        } catch (\Exception $e) {
            echo $e->getMessage(), "<br />";
            exit();
        }
        $v = new TweeterView([], $this->router);
        $v->render("renderFollowers");

    }

    public function logout(){
        $auth = new Authentification();
        $auth->logout();
        header('Location: '.$this->router->urlFor("maison"));
    }

    public function viewSignup(){
        $v = new TweeterView([], $this->router);
        $v->render("renderSignup");
    }

    public function checkSignup(){

            if(empty($_POST["username"])|| empty($_POST["fullname"]) || empty($_POST["password"])){
                echo"Vous devez remplir tous les champs";
                die();
            }
            if($_POST["password"] != $_POST["password_confirm"]){
                echo"Les mots de passe ne correspondent pas.";
                die();
            }
            $auth = new TweeterAuthentification();

            try{
                $auth->createUser($_POST["username"], $_POST["password"], $_POST["fullname"], TweeterAuthentification::ACCESS_LEVEL_USER);
            }
            catch (\Exception $e){
                echo $e->getMessage(), "<br />";
               die();
            }
            header('Location: '.$this->router->urlFor("maison"));
    }

    public function viewFollowing(){
        $auth = new Authentification();
        $user =  User::all()->where("username", "=", $auth->user_login);
        $users_list = $user->first()->follow()->get();
        $v = new TweeterView($users_list, $this->router);
        $v->render("renderFollowing");
    }

    public function viewFollow(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();
        $userToFollow = User::find($this->request->get["id"]);
        $followExists = Follow::all()->where("follower", "=", $connectedUser->id)->where("followee", "=", $userToFollow->id)->first();

        if(!$followExists){
            $follow = new Follow();
            $follow->follower = $connectedUser->id;
            $follow->followee = $userToFollow->id;
            $follow->save();
        }
        header('Location: '.$this->router->urlFor("user", ["id" => $userToFollow->id]));
    }

    public function viewUnfollow(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();
        $userToUnfollow = User::find($this->request->get["id"]);
        $followExists = Follow::all()->where("follower", "=", $connectedUser->id)->where("followee", "=", $userToUnfollow->id)->first();

        if($followExists){
            $followExists->delete();
        }
        header('Location: '.$this->router->urlFor("user", ["id" => $userToUnfollow->id]));
    }
}
