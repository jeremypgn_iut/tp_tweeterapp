<?php
namespace tweeterapp\control;

use mf\auth\Authentification;
use tweeterapp\model\Like;
use tweeterapp\model\Tweet;
use tweeterapp\model\User;
use tweeterapp\view\TweeterView;

class TweeterController extends \mf\control\AbstractController {

    public function __construct($router){
        parent::__construct($router);

    }

    public function viewHome(){

        $v = new TweeterView(Tweet::orderBy("created_at", "DESC")->get(), $this->router);
        $v->render("renderHome");

    }

    public function viewTweet(){

        if(!isset($this->request->get["id"]) || !is_numeric($this->request->get["id"])){
            die("404");
        }
        $v = new TweeterView(Tweet::all()->where("id", "=", $this->request->get["id"])->first(), $this->router);
        $v->render("renderViewTweet");
    }

    public function viewUserTweets(){

        if(!isset($this->request->get["id"]) || !is_numeric($this->request->get["id"])){
            die("404");
        }

        $v = new TweeterView(User::all()->where("id", "=" , $this->request->get["id"])->first(), $this->router);
        $v->render("renderUserTweets");
    }

    public function viewFormTweet(){
        $v = new TweeterView([], $this->router);
        $v->render("renderPostTweet");

    }

    public function postTweet(){
        $post = $this->request->post;
        if(strlen($post["text"]) > 140){
            $this->viewFormTweet();
            echo "<script>alert('Votre tweet ne doit pas faire plus de 140 caractères.')</script>";
        }
        else if(empty(trim($_POST["text"]))){
            $this->viewFormTweet();
            echo "<script>alert('Votre tweet ne doit pas être vide.')</script>";
        }else{
            $auth = new Authentification();
            $user_id = User::all()->where("username", "=", $auth->user_login)->first()->getAttributes()["id"];

            $tweet = new Tweet();
            $tweet->text = htmlentities($post["text"]);
            $tweet->author = $user_id;
            $tweet->score = 0;

            $tweet->save();
            header('Location: '.$this->router->urlFor("user", ["id" => $user_id]));
        }
    }

    public function like(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();

        $tweetToLike = Tweet::find($this->request->get["id"]);
        $likeExists = Like::all()->where("user_id", "=", $connectedUser->id)->where("tweet_id", "=", $tweetToLike->id)->first();
        if(!$likeExists){
            $like = new Like();
            $like->user_id = $connectedUser->id;
            $like->tweet_id = $tweetToLike->id;
            $like->save();
        }
        header('Location: '.$this->router->urlFor("view", ["id" => $tweetToLike->id]));
    }

    public function disklike(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();
        $tweetToDislike = Tweet::find($this->request->get["id"]);

        $likeExists = Like::all()->where("user_id", "=", $connectedUser->id)->where("tweet_id", "=", $tweetToDislike->id)->first();
        if($likeExists){
            $likeExists->delete();
        }
        header('Location: '.$this->router->urlFor("view", ["id" => $tweetToDislike->id]));
    }

    public function viewFollowingTweets(){
        $auth = new Authentification();
        $user =  User::all()->where("username", "=", $auth->user_login);
        $users_list = $user->first()->follow()->get();
        $users_id = [];

        foreach ($users_list as $user){
            $users_id[] = $user->id;
        }
        $tweets = Tweet::whereIn('author', $users_id)->get();

        $v = new TweeterView($tweets, $this->router);
        $v->render("renderFollowingTweets");
    }
}
