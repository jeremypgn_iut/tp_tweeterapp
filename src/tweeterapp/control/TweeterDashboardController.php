<?php
namespace tweeterapp\control;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Capsule\Manager as DB;
use mf\auth\Authentification;
use mf\control\AbstractController;
use tweeterapp\auth\TweeterAuthentification;
use tweeterapp\model\Follow;
use tweeterapp\model\User;
use tweeterapp\view\TweeterView;

class TweeterDashboardController extends AbstractController {
    public function __construct($router){
        parent::__construct($router);

    }

    public function viewUsers(){

        //Je récupére tous les utilisateurs
        $users = User::all();

        // Je crée un tableau pour commencer le trie
        $users_with_followers_nb = [];

        // Je boucle sur tous les utilisateurs et j'ajoute dans le tableau $users_with_followers_nb un tableau avec l'objet User et le nombre de followers
        foreach ($users as $user) {
            $users_with_followers_nb[] = [$user, Follow::where("followee", "=", $user->id)->count()];
        }

        //Je crée un tableau qui accueilera le nombre de followers de chaque utilisateurs
        $followers = [];

        foreach ($users_with_followers_nb as $key => $row)
        {
            $followers[$key] = $row[1];
        }

        //Je trie le tableau
        array_multisort($followers, SORT_DESC, $users_with_followers_nb);

        //Je rends la vue avec le tableau trié
        $v = new TweeterView($users_with_followers_nb, $this->router);
        $v->render("renderUsers");
    }

    public function viewFollowersByUser(){

        // Je récupére l'utilisateur et je verifie si il existe
        $user = User::find($_GET["id"]);
        if(!$user){
            die("Utilisateur introuvable");
        }
        // Je récupére les followers de l'utilisateur
        $followers = $user->followedBy()->get();

        $sphere = $followers->toArray();

        foreach ($followers as $follower){


            if(!in_array($follower->toArray(), $sphere)){

                $sphere[] = $follower;
            }


            foreach ($follower->followedBy()->get()->toArray() as $cousin_follower){

                if(!in_array($cousin_follower, $sphere)){

                    $sphere[] = $cousin_follower;
                }

            }


        }



        //Je rends la vue avec l'utilisateur et la liste de ses followers
        $v = new TweeterView(["user" => $user, "followers" => $followers, "sphere" => $sphere], $this->router);
        $v->render("renderFollowersByUser");
    }
}
