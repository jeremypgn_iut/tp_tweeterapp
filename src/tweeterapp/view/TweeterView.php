<?php

namespace tweeterapp\view;

use mf\auth\Authentification;
use tweeterapp\model\Follow;
use tweeterapp\model\Like;
use tweeterapp\model\Tweet;
use tweeterapp\model\User;

class TweeterView extends \mf\view\AbstractView {
    static protected $style_sheets = ["html/css/style.css"];

    public function __construct( $data, $router){
        parent::__construct($data, $router);
    }

    private function renderHeader(){
        $auth = new Authentification();

        $html = '<h1>MiniTweeTR</h1><nav><ul>';
        $html .= '<li><a href="'.$this->router->urlFor("maison").'"><img src="'.$this->router->getRoot().'/html/img/home.png" width="30"></a></li>';
          if($auth->logged_in){
              $html .= '<li><a href="'.$this->router->urlFor("logout").'"><img src="'.$this->router->getRoot().'/html/img/logout.png" width="30"></a></li>';
              $html .= '<li><a href="'.$this->router->urlFor("post").'"><img src="'.$this->router->getRoot().'/html/img/add.png" width="30"></a></li>';
              $html .= '<li><a href="'.$this->router->urlFor("following").'"><img src="'.$this->router->getRoot().'/html/img/followees.png" width="30"></a></li>';
              $html .= '<li><a href="'.$this->router->urlFor("following_tweets").'"><img src="'.$this->router->getRoot().'/html/img/wall.png" width="30"></a></li>';
          }else{
              $html .= '<li><a href="'.$this->router->urlFor("login").'"><img src="'.$this->router->getRoot().'/html/img/login.png" width="30"></a></li>';
          }
        $html .= '</ul></nav>';

        return $html;
    }

    private function renderFooter(){
        return 'La super app créée en Licence Pro &copy;'.date("Y");
    }

    function renderHome(){

        $html = "
<article>
<h2>Les derniers tweets</h2>

";

        foreach ($this->data as $tweet){
            $html .= $this->renderViewTweet($tweet);
        }
        $html .= "</article>";
        return $html;
        
    }

    function renderFollowingTweets(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();

        $followers = Follow::where('followee', '=', $connectedUser->id)->get();

        if($followers->count() > 1){
            $nb_followers = $followers->count()." followers";
        }else{
            $nb_followers = $followers->count()." follower";
        }

        $html = "
<article>
<h2>Vous avez ".$nb_followers." :</h2><ul>";
        foreach ($followers as $follower){
            $user = User::find($follower->follower);
            $html .= "<li>".$user->fullname." (<a href='".$this->router->urlFor("user", ["id" => $user->id])."'>".$user->username."</a>)</li>";
        }
$html .= "</ul><h2>Tweets de vos abonnements</h2>";

        foreach ($this->data as $tweet){
            $html .= $this->renderViewTweet($tweet);
        }
        $html .= "</article>";
        return $html;

    }

    private function renderUserTweets(){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();

        $user = $this->data;
        $tweets = $user->tweets()->orderBy("created_at", "DESC")->get();
        $html = "";

        $nb_followers = Follow::where('followee', '=', $user->id)->count();
        if($nb_followers > 1){
            $nb_followers .= " followers";
        }else{
            $nb_followers .= " follower";
        }
        $html .= "
<article>
<h2>Profil de ".$user->fullname." (".$nb_followers.")</h2>
";
        if($connectedUser){
            if($user->id != $connectedUser->id){
                $isFollowed = Follow::all()->where("follower", "=", $connectedUser->id)->where("followee", "=", $user->id)->first();
                if(!$isFollowed){
                    $html .= "<a href='".$this->router->urlFor("follow", ["id" => $user->id])."'>Suivre</a>";
                }else{
                    $html .= "<a href='".$this->router->urlFor("unfollow", ["id" => $user->id])."'>Ne plus suivre</a>";
                }
            }
        }

        foreach ($tweets as $tweet) {
            $html .= $this->renderViewTweet($tweet);
        }

        $html .= "</article>";

        return $html;
    }

    private function renderViewTweet($tweet = []){
        $auth = new Authentification();
        $connectedUser =  User::all()->where("username", "=", $auth->user_login)->first();

        if(!$tweet){
            $tweet = $this->data;
        }
        $author = $tweet->author()->first();
        $likes = Like::all()->where("tweet_id", "=", $tweet->id)->count();
        if($likes > 1){
            $url_action = $likes." likes";
        }else{
            $url_action = $likes." like";
        }


        if($connectedUser){
            if(Like::all()->where("tweet_id", "=", $tweet->id)->where("user_id", "=", $connectedUser->id)->first()){
                $url_action .= " <a href='".$this->router->urlFor("dislike", ["id" => $tweet->id])."'><img src='".$this->router->getRoot()."/html/img/liked.png' width='15'></a>";
            }else{
                $url_action .= " <a href='".$this->router->urlFor("like", ["id" => $tweet->id])."'><img src='".$this->router->getRoot()."/html/img/like.png' width='15'></a>";
            }
        }


        $html = "
  <div class='tweet'>         
    <div class='tweet-text'><a href='".$this->router->urlFor("view", ["id" => $tweet->id])."'>".$tweet->text."</a></div>
    <div class='tweet-author'><a href='".$this->router->urlFor("user", ["id" => $author->id])."'>".$author->fullname."</a></div>
    <div class='tweet-footer'>".$tweet->created_at." ($url_action)</div>
  </div>            
            ";
        return $html;
    }

    protected function renderPostTweet(){
        return '<article><h2>Ajouter un tweet</h2><form method="post" action="'.$this->router->urlFor("send").'" class="addTweet">
    <textarea placeholder="Tweet..." name="text">'.@$_POST["text"].'</textarea>
    <input type="submit" />
</form></article>';
        
    }

    protected function renderLogin(){
        $post_url = $this->router->urlFor("check_login");
        $signup_url = $this->router->urlFor("signup");
        return <<<EOT
<article><h2>Connexion</h2><form action="${post_url}" method="post">
   <input type="text" name="username" placeholder="Nom d'utilisateur"/><br />
   <input type="password" name="password" placeholder="********"/><br />
   <input type="submit" value="Se connecter"/>
</form><br /><a href="${signup_url}">Inscrivez-vous</a></article>
EOT;
    }

    protected function renderFollowers(){
        $auth = new Authentification();
        $username = $auth->user_login;

        $user =  User::all()->where("username", "=", $username);
        $followers = $user->first()->followedBy()->get();
        $html = "<article><h2>Followers de ".$user->first()->username."</h2><ul>";
        foreach ($followers as $follower){
            $html .= "<li>".$follower->fullname." (<a href='".$this->router->urlFor("user", ["id" => $follower->id])."'>".$follower->username."</a>)</li>";
        }
        $html .= "</ul></article>";

        return $html;
    }

    protected function renderSignup(){
        $post_url = $this->router->urlFor("check_signup");
        return <<<EOT
<article><h2>Inscription</h2><form action="${post_url}" method="post">
   <input type="text" name="username" placeholder="Nom d'utilisateur"/><br />
   <input type="text" name="fullname" placeholder="Nom complet"/><br />
   <input type="password" name="password" placeholder="********"/><br />
   <input type="password" name="password_confirm" placeholder="********"/><br />
   <input type="submit" value="S'inscrire"/>
</form></article>
EOT;
    }

    protected function renderFollowing(){
        $following = $this->data;
        $html = "<article><h2>Utilisateurs suivis</h2><ul>";
        foreach ($following as $user){
            $html .= "<li>".$user->getAttributes()["fullname"]." (<a href='".$this->router->urlFor("user", ["id" => $user->getAttributes()["id"]])."'>".$user->getAttributes()["username"]."</a>)</li>";
        }
        $html .= "</ul></article>";

        return $html;
    }

    protected function renderUsers(){
        $html = "<article><h2>Utilisateurs</h2>";
        foreach ($this->data as $user){
            $followers = Follow::where('followee', '=', $user[0]->id)->get();
            $html .= "<li>".$user[0]->fullname." (<a href='".$this->router->urlFor("followers_per_user", ["id" => $user[0]->id])."'>".$user[0]->username."</a>) - ".$followers->count()." followers</li>";
        }

        $html .= "</article>";

        return $html;
    }

    protected function renderFollowersByUser(){
        $html = "<article><h2>Followers de ".$this->data["user"]->username."</h2><ul>";

        foreach ($this->data["followers"] as $follower){
            $html .= "<li>".$follower->fullname." (<a href='".$this->router->urlFor("user", ["id" => $follower->id])."'>".$follower->username."</a>)</li>";
        }
        $html .= "</ul><h2>Sphère d'influence</h2><ul>";


        foreach ($this->data["sphere"] as $follower){
            $html .= "<li>".$follower["fullname"]." (<a href='".$this->router->urlFor("user", ["id" => $follower["id"]])."'>".$follower["username"]."</a>)</li>";
        }
        $html .= "</ul>";


        return $html;
    }

    protected function renderBody($selector=null){

        return $this->$selector();
    }

    public function render($selector){
        /* le titre du document */
        $title = self::$app_title;

        /* les feuilles de style */
        $app_root = (new \mf\utils\HttpRequest())->root;
        $styles = '';
        foreach ( self::$style_sheets as $file )
            $styles .= '<link rel="stylesheet" href="'.$app_root.'/'.$file.'"> ';

        /* on appele la methode renderBody de la sous classe */
        $body = $this->renderBody($selector);
        $header = $this->renderHeader();
        $footer = $this->renderFooter();
        /* construire la structure de la page
         *
         *  Noter l'utilisation des variables ${title} ${style} et ${body}
         *
         */

        $html = <<<EOT
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>${title}</title>
	    ${styles}
    </head>

    <body>
    <header class="theme-backcolor1">${header}</header>
       <section class="theme-backcolor2">
       ${body}
        </section> 
    <footer class="theme-backcolor1">${footer}</footer>
    </body>
</html>
EOT;
        echo $html;
    }




    
}
