# TweeterApp

TweeterApp est une mini application Tweeter. Cet outil doit permettre aux utilisateur de créer des Tweets et parcourir les Tweets existants.

## Exigences

Utilisez [composer](https://getcomposer.org/) pour installer le paquet "illuminate/database"

```bash
composer require illuminate/database
```

## Licence
[MIT](https://choosealicense.com/licenses/mit/)