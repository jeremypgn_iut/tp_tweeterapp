<?php
session_start();
use mf\router\Router;
use tweeterapp\auth\TweeterAuthentification;
use tweeterapp\model\Tweet;
use tweeterapp\model\User;

require_once 'vendor/autoload.php';
require_once "src/mf/utils/ClassLoader.php";

// Définition des constantes
define("TITLE", "Tweeter");
define("DB_CONFIG", parse_ini_file("conf/db.ini"));

// Chargement des classes
$loader = new mf\utils\ClassLoader("src");
$loader->register();

// Initialisation de la base de données
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection(DB_CONFIG);
$db->setAsGlobal();
$db->bootEloquent();

// Liste des utilisateurs

/*$users = User::all();
foreach ($users as $user){
    var_dump($user->getAttributes());
}*/

// Liste des tweets

/*$tweets = Tweet::all();
foreach ($tweets as $tweet){
    var_dump($tweet->getAttributes());
}*/

// Liste des tweets ordonnés par date de modification

/*$tweets = Tweet::all()->sortBy("updated_at");
foreach ($tweets as $tweet){
    var_dump($tweet->getAttributes());
}*/

// Liste des tweets avec un score positif

/*$tweets = Tweet::all()->where("score", ">", 0);
foreach ($tweets as $tweet){
    var_dump($tweet->getAttributes());
}*/

//Ajout d'un tweet

/*$tweet = new Tweet();
$tweet->text = "Mon premier tweet !";
$tweet->author = 1;
$tweet->score = 0;

$tweet->save();*/

//Ajout d'un utilisateur

/*$user = new User();
$user->fullname = "Jérémy Pingeon";
$user->username = "jerem";
$user->password = "";
$user->level = 100;
$user->followers = 0;

$user->save();*/

// Lecture de l'auteur d'un tweet

/*$tweet = Tweet::all()->where("id", "=", "49");
var_dump($tweet->first()->author()->first());*/

// Lecture des tweets d'un auteur

/*$user = User::all()->where("id", "=" , 1)->first();
$tweets = $user->tweets()->get();
foreach ($tweets as $tweet) {
    var_dump($tweet);
}*/

// Utilisateurs qui ont likés un tweet
/*$tweet = Tweet::all()->where("id", "=", "63");
$users_list = $tweet->first()->likedBy()->get();

foreach ($users_list as $user) {
    var_dump($user->getAttributes());
}*/

// Tweets appréciés par un utilisateur
/*$user =  User::all()->where("id", "=", "10");
$tweets_list = $user->first()->liked()->get();
foreach ($tweets_list as $tweet){
    var_dump($tweet->getAttributes());
}*/

// Utilisateurs qui suivent un utilisateur
/*$user =  User::all()->where("id", "=", "10");
$users_list = $user->first()->followedBy()->get();
foreach ($users_list as $user){
    var_dump($user->getAttributes());
}*/

// Utilisateurs suivis par un utilisateur
/*$user =  User::all()->where("id", "=", "9");
$users_list = $user->first()->follow()->get();
foreach ($users_list as $user){
    var_dump($user->getAttributes());
}*/



$router = new \mf\router\Router();

$router->addRoute('maison',
    '/home/',
    '\tweeterapp\control\TweeterController',
    'viewHome');

$router->addRoute('login',
    '/login/',
    '\tweeterapp\control\TweeterAdminController',
    'viewLogin');

$router->addRoute('logout',
    '/logout/',
    '\tweeterapp\control\TweeterAdminController',
    'logout');

$router->addRoute('check_login',
    '/check_login/',
    '\tweeterapp\control\TweeterAdminController',
    'checkLogin');

$router->addRoute('signup',
    '/signup/',
    '\tweeterapp\control\TweeterAdminController',
    'viewSignup');

$router->addRoute('check_signup',
    '/check_signup/',
    '\tweeterapp\control\TweeterAdminController',
    'checkSignup');

$router->addRoute('view',
    '/view/',
    '\tweeterapp\control\TweeterController',
    'viewTweet');

$router->addRoute('like',
    '/like/',
    '\tweeterapp\control\TweeterController',
    'like');

$router->addRoute('dislike',
    '/dislike/',
    '\tweeterapp\control\TweeterController',
    'disklike');

$router->addRoute('user',
    '/user/',
    '\tweeterapp\control\TweeterController',
    'viewUserTweets');

$router->addRoute('following',
    '/following/',
    '\tweeterapp\control\TweeterAdminController',
    'viewFollowing', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('follow',
    '/follow/',
    '\tweeterapp\control\TweeterAdminController',
    'viewFollow', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('unfollow',
    '/unfollow/',
    '\tweeterapp\control\TweeterAdminController',
    'viewUnfollow', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute("post",
    '/post/',
    '\tweeterapp\control\TweeterController',
    'viewFormTweet', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute("following_tweets",
    '/following_tweets/',
    '\tweeterapp\control\TweeterController',
    'viewFollowingTweets', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute("send",
    '/send/',
    '\tweeterapp\control\TweeterController',
    'postTweet', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute("users",
    '/dashboard/users/',
    '\tweeterapp\control\TweeterDashboardController',
    'viewUsers', TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('followers_per_user',
    '/dashboard/followers_per_user/',
    '\tweeterapp\control\TweeterDashboardController',
    'viewFollowersByUser');

$router->setDefaultRoute('/home/');

$router->run();
